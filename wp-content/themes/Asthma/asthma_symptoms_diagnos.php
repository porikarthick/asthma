<?php get_header(); ?>
	<div class="ASTHMASYMPTOMS&DIAGNOSISdiv">
		<div class="container-fluid bannerimg-contantdiv">
			<div class="col-md-6 col-sm-6 col-xs-12 imgcontantdiagnosisdiv">
				<img class="img-responsive center-block" src="<?php echo get_template_directory_uri();?>/images/page2_banner.jpg"></a>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 contantdiagnosisdiv">
				<div class="contant">
					<img class="img-responsive center-block liver" src="<?php echo get_template_directory_uri();?>/images/liver-red.png"></a>
					
						<h2 class="h2asthmasymptomsclass">Asthma Symptoms</h2>
					<div class="paddingclass">
						<h4 class="h4asthmasymptomsclass">Common symptoms of asthma include:</h4>
						<p class="pasthmasymptomsclass">• Coughing</p>
						<p class="pasthmasymptomsclass">• Wheezing (a whistling, squeaky sound when you breathe)</p>
						<p class="pasthmasymptomsclass">• Shortness of breath</p>
						<p class="pasthmasymptomsclass">• Rapid breathing</p>
						<p class="pasthmasymptomsclass">• Chest tightness</p>

					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid DIAGNOSIScontantdiv">
			<div class="diagnosisheaderclass">
				<img class="img-responsive center-block liver" src="<?php echo get_template_directory_uri();?>/images/diagnosis.png"></a>
				<h1 class="h1diagnosisclass">Asthma Diagnosis</h1>
			</div>
			<div class="container bodydiagnosiscontainer">
				<p class="pdialogclass">To diagnose asthma, your doctor will discuss your medical history and symptoms with you. They will also perform a physical exam. You may also take lung function tests.</p>
				<h3><span class="clrspan">Common tools to diagnose asthma include:</span></h3>
				<div class="col-md-12 col-sm-12 cont-image-div">
					<div class="col-md-2 col-sm-2 image-div">
						<img class="img-responsive center-block " src="<?php echo get_template_directory_uri();?>/images/personal.png"></a>
					</div>
					<div class="col-md-10 col-sm-10 cont-div">
						<p class="pdialogclass1"><span class="clrspan">Personal and medical history.</span> Your doctor will ask you questions to understand your symptoms and their causes.Bring notes to help jog your memory. Be ready to answer questions about your family history, the medicines you takeand your lifestyle. This includes any current physical problems. Shortness of breath, wheezing, coughing andtightness in your chest may be due to asthma, but can be due to other diseases as well.</p>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 cont-image-div">
					<div class="col-md-2 col-sm-2 image-div">
						<img class="img-responsive center-block " src="<?php echo get_template_directory_uri();?>/images/physical.png"></a>
					</div>
					<div class="col-md-10 col-sm-10 cont-div">
						<p class="pdialogclass1"><span class="clrspan">Physical examination.</span> If your doctor thinks you have asthma, they will do a physical exam. They will look at your ears,eyes, nose, throat, skin, chest and lungs. This exam may include a lung function test to see how well you exhale air from your lungs. It may also include a skin test or an X-ray of your lungs or sinuses. A physical exam then allows yourdoctor to review your health.</p>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 cont-image-div">
					<div class="col-md-2 col-sm-2 image-div">
						<img class="img-responsive center-block " src="<?php echo get_template_directory_uri();?>/images/lungs.png"></a>
					</div>
					<div class="col-md-10 col-sm-10 cont-div">
						<p class="pdialogclass1"><span class="clrspan">Lung function tests.</span> To confirm asthma, your doctor may have you take one or more breathing tests known as lung function tests. These tests measure your breathing. Lung function tests are often done before and after inhaling a medicine known as a bronchodilator (brahn-koh-DIE-ah-lay-ter), which opens your airways. If your lung function
						improves a lot with the use of a bronchodilator, you probably have asthma. Your doctor may also prescribe a trial of asthma medicine to see if it helps. Common lung function tests used to diagnose asthma include:</p>
						<div class="subcontantdiv">
							<ul>
								<li><p class="pdialogclass1"><span class="clrspan">• Spirometry.</span> This is the recommended test to confirm asthma. During this test, you breathe into a mouthpiece
								that’s connected to a device. It is called a spirometer. The spirometer measures the amount of air you’re able to breathe in and out and its rate of flow. You will take a deep breath and then exhale forcefully.</p>
								</li>
								<li><p class="pdialogclass1"><span class="clrspan">• Peak airflow.</span> This test uses a peak flow meter. It’s a small, handheld device that you breathe into to measure
								the rate at which you can force air out of your lungs. During the test, you breathe in as deeply as you can and then blow into the device as hard and fast as possible. If you’re diagnosed with asthma, you can use a peak flow meter at home to help track your condition.</p></li>
								<li><p class="pdialogclass1"><span class="clrspan">• Methacholine challenge.</span> If your symptoms and spirometry don’t give your doctor a clear answer to make an asthma diagnosis, methacholine (meh-thah-KOH-leen) testing can be used to rule out asthma. A negative methacholine challenge is a strong indicator that asthma is not the correct diagnosis. In bronchial asthma, your airways will be hyperactive, or extra sensitive. This sets asthma apart from other conditions. A methacholine challenge test checks how sensitive airways are in people with respiratory symptoms, such as coughing and
								chest tightness, when the diagnosis of asthma is uncertain. It is also called a bronchoprovocation
								(brahn-koh-proh-voh-KA-shun) test.</p></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>		