<footer>
	<div class="container-fluid footerdiv">
		<div class="container holefoterdiv">
			<div class="col-md-9 col-sm-8 col-xs-12 fotercontantdiv">
				<h5 class="hreferenceclass">Reference</h5>
				<p> 1. Shawn D. Aaron, Katherine L. Vandemheen, J. Mark FitzGerald, Martha Ainslie, Samir Gupta, Catherine Lemière, Stephen K. Field, R. Andrew McIvor,Paul Hernandez, Irvin Mayers, Sunita Mulpuru, Gonzalo G. Alvarez, Smita Pakhale, Ranjeeta Mallick, Louis-Philippe Boulet. Reevaluation of Diagnosis in Adults With Physician-Diagnosed Asthma. JAMA. 2017;317(3):269–279. doi:10.1001/jama.2016.19627.</p>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12 fotersociallogodiv">
				<div class="col-md-6 col-sm-6 col-xs-6 twiterlogodiv">
					<img class="img-responsive center-block " src="<?php echo get_template_directory_uri();?>/images/twitter.png"></a>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 fblogodiv">	
					<img class="img-responsive center-block  " src="<?php echo get_template_directory_uri();?>/images/fb.png"></a>
				</div>
			</div> 
		</div>
	</div>
		
</footer>
</body>
</html>