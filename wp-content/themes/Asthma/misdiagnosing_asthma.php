<?php get_header(); ?>
	<div class="misidiagnosingdiv">
		
			<div class="col-md-12 col-sm-12 col-xs-12 misidiagnosingholecol">
				<div class="col-md-6 col-sm-6 col-xs-12 misidiagnosingleftcol">
					<div class="leftsidemisidiagnosingcont">
						<img class="img-responsive center-block liver" src="<?php echo get_template_directory_uri();?>/images/liver-red.png"></a>
						
							<h2 class="h2misclass"><span class="clrspan">Asthma-Like Conditions</span></h2>
						<p class="pdialogclass1">Because there are so many conditions that have some of the
						same symptoms of asthma, it’s very important to make sure you’re getting the right diagnosis.</p>
						<p class="pdialogclass1">Here are some other health issues that can cause symptoms similar to asthma:</p>
						<ul class="ulclass2">
							<li class="sidelistclass">• Pneumonia</li></br>
							<li class="sidelistclass">• Allergies</li></br>
							<li class="sidelistclass">• Upper airway obstruction</li></br>
							<li class="sidelistclass">• Heart failure</li></br>
							<li class="sidelistclass">• Vocal cord dysfunction</li></br>
							<li class="sidelistclass">• Bronchopulmonary dysplasia</li></br>
							<li class="sidelistclass">• Cystic fibrosis</li></br>
							<li class="sidelistclass">• Enlarged lymph nodes</li></br>
							<li class="sidelistclass">• Lung cancer (tumors)</li></br>
							<li class="sidelistclass">• Viral infections</li></br>
							<li class="sidelistclass">• Sinus infections</li></br>
							<li class="sidelistclass">• Chronic Obstructive Pulmonary Disease
(emphysema or chronic bronchitis)</li></br>
							<li class="sidelistclass">• Bronchiolitis</li></br>
							<li class="sidelistclass">• Pulmonary embolism</li></br>
							<li class="sidelistclass">• Recurrent cough not due to asthma</li></br>
							<li class="sidelistclass">• Transient wheeze not due to asthma</li></br>
							<li class="sidelistclass">• Acid reflux (gastrointestinal reflux)</li></br>
							<li class="sidelistclass">• Obesity</li></br>
							<li class="sidelistclass">• Heart attack</li></br>
							<li class="sidelistclass">• Hiatal hernia</li></br>
						</ul>
						<p class="pdialogclass1">If your asthma diagnosis is in question, take advantage of
						objective tests to confirm or rule out an asthma diagnosis. Don’t							be afraid to ask your doctor questions. If your treatment plan							doesn’t seem to be working, let your doctor know.</p>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 misidiagnosingrightcol">
					<div class="rightsideemisidiagnosingimg">
						<img class="img-responsive center-block " src="<?php echo get_template_directory_uri();?>/images/page3-banner2.jpg"></a>
					</div>
					<div class="rightsideemisidiagnosingcont">
						<div class="headeremisidiagnosinglogo">
							<img class="img-responsive center-block " src="<?php echo get_template_directory_uri();?>/images/diagnosis-white.png"></a>
							<h1 class="h1classmi">Misdiagnosing Asthma</h1>
							<div class="lastcontantclass">
								<p class="pdialogclass2">Whether you have asthma or another condition, the right
								diagnosis means better quality of life. Following the right treatment plan can improve your affect you physical, mental and financial well-being.</p>
								<p class="pdialogclass2">With some conditions, the right diagnosis can be the difference between life and death. Some conditions need to be treated quickly and properly. If you have been misdiagnosed with asthma, you may be using long-term medicines that are not helpful. You would not be receiving treatment for the true cause of your symptoms. Taking certain unnecessary asthma medicines (such as steroids) may be harmful over extended periods of time. Misdiagnosis can also lead to unnecessary dollars and time spent managing and treating the wrong problem.</p>
								<p class="pdialogclass2">Your doctor and you both have the same goal: for you to feel well and be healthy. Partner with your doctor to verify your asthma diagnosis for better health.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
	
	</div>


<?php get_footer(); ?>