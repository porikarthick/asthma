<?php get_header(); ?>
	<div class="whatisasthmadiv">
		<div class="container-fluid bannerdiv">
			<!-- <img class="img-responsive center-block" src="<?php //echo get_template_directory_uri();?>/images/banner.jpg"></a> -->
			<div class="container">
				<div class="col-md-4 col-sm-5 col-xs-5 contdetails">
<!-- 					<div class="detailscont">
						<h2>Is It Asthma?</h2>
						<h4>DOWNLOAD THE GUIDE</h4>
					</div> -->
				</div>
				<div class="col-md-8 col-sm-7 col-xs-7 contdetails2">
				</div>
			</div>
		</div>
		<div class="container-fluid introcontantdiv">
			<div class="container introcontantcontainerdiv">
				<img class="img-responsive center-block" src="<?php echo get_template_directory_uri();?>/images/liver-red.png"></a>
				<h2><span class="clrspan">An Introduction to Asthma</span></h2>
				<p class="pclrclass"><span class="clrspan">Asthma</span> is a chronic disease that causes your airways to become inflamed, making it hard to breathe.</p>
				<p class="pclrclass">You probably know the <span class="clrspan">common symptoms of asthma</span>, like shortness of breath, wheezing and coughing.But did you know there are other respiratory illnesses with similar symptoms?</p>
				<p class="pclrclass"><span class="clrspan">Diagnosing asthma</span> can be trickier than you think. There is no single test to determine if you have asthma.To make a diagnosis, your doctor will look at your symptoms and may perform some physical tests.</p>
			</div>
		</div>
		<div class="container-fluid resardiv">
			<div class="col-md-12 col-sm-12 col-xs-12 resargriddiv">
				<div class="col-md-6 col-sm-6 col-xs-12 leftsidecont">
					<div class="col-md-6 col-sm-6 col-xs-12 innerleftsidecont">
						<img class="img-responsive center-block" src="<?php echo get_template_directory_uri();?>/images/user.png"></a>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 innerleftsidecont">
						<p class="pclrclass2">In some cases, people are
							misdiagnosed with asthma when
							they actually have a different
							condition. Recently, Canadian
							researchers studied more than 600
							adults diagnosed with asthma in
							the past 5 years. <span class="spanyellowclrclass">They found that
							about one-third of them didn’t
							truly have asthma.1</span>
						</p>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 rightsidecont">
					<div class="rightsidecontinnerclass">
						<h1 class="h1clrclass">So what does this mean for you?</h1>
						<p class="pclrclass2">If you are on a treatment plan for asthma but the treatment
							doesn’t seem to be working, you might not have asthma. Or you
							may need a different type of asthma medicine.
						</p>
						<p class="pclrclass2">When it comes to managing your health, you want the best
							treatment possible.<span class="spanyellowclrclass"> This means you have to start by getting the
							right diagnosis for your symptoms.</span>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid endigcontantdiv">
			<div class="container endingcontantcontainerdiv">
				<div class="col-md-2 col-sm-2 emptycont">
					gfkj
				</div>
				<div class="col-md-8 col-sm-8 lastcontantdiv">
					<img class="img-responsive center-block" src="<?php echo get_template_directory_uri();?>/images/diagnosis.png">
					<h2>To get the right care, you need the right diagnosis.</h2>
					<p>It might be obvious, but for asthma treatments to work, you truly need to have asthma. If
					you’ve been misdiagnosed, you might be trying to treat a condition you don’t have – and at
					the same time possibly ignoring a condition you really have!</p>
					<p>Work with your doctor to get the right diagnosis. If you’ve been told you have asthma but
					have not had objective testing, see a board-certified allergist, immunologist or</p>
					<p>ytdiygikgkiugfhggjcy</p>
				</div>
				<div class="col-md-2 col-sm-2 emptycont">
					fghd
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>