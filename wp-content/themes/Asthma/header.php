<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">	
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css">
	<script src="<?php echo get_template_directory_uri();?>/js/jquery.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/bootstrap.min.js"></script>
</head>
<body>
	<header class="headerclas">
		<div class="container">
			<div class="col-md-4 col-sm-4 padd-zero">				
					<img class="img-responsive center-block logo1" src="<?php echo get_template_directory_uri();?>/images/logo.png"></a>				
			</div>
			<div class="col-md-8 col-sm-8 padd-zero">
				<div class="col-md-12  padd-zero">
					<img class="img-responsive center-block pull-right right-image-mob logo2" src="<?php echo get_template_directory_uri();?>/images/top-right-logo.png"></a>
				</div>
				<div class="col-md-12 col-sm-12 padd-zero1">
					<nav class="navbar navbar-inverse navbg">
						
					  <div class="container-fluid padd-zero">
						    <div class="navbar-header">
						      <a class="navbar-brand" href="http://localhost/asthma"><img class="logo" src="<?php echo get_template_directory_uri();?>/images/top-right-logo.png"></a>
						      <button type="button" class="navbar-toggle btn" data-toggle="collapse" data-target="#myNavbar">
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>
						        <span class="icon-bar"></span>                        
						      </button>
						      
						    </div>
						    <div class="collapse navbar-collapse padd-zero" id="myNavbar">
						      <ul class="nav navbar-nav navbar-right">
						      	<?php wp_nav_menu( array(
					   				'menu'           => 'mainmenu', // Do not fall back to first non-empty menu.
					    			'theme_location' => '__no_such_location',
					    			'fallback_cb'    => 'wp_page_menu' // Do not fall back to wp_page_menu()
								) ); ?>
						        
						      </ul>
						    </div>
					  </div>
					</nav>
				</div>
			</div>
		</div>
	</header>