<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asthma');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?u/uzjY<k=Vd~)e Tz:=*E!+qigys!C~5}GD`*.]:)3+SBS{N[1^J&#=quX3F<Zy');
define('SECURE_AUTH_KEY',  'D`1d1<S$ }J4`EGpCS[d:PtI<]2%YCsSXOtMn-5b/rpS1sIpqjf4f7E^JHxs>lS>');
define('LOGGED_IN_KEY',    'F^+uA`OoiObB-l3!A8.E*v5%0[D^3/o;kyHX}M$.^s6wgpC^+~,_w$(YGrgr*%k@');
define('NONCE_KEY',        'O:Vr{K>=VMo!WM[@98d,OyuPqlus%1T!I0FS.{1)#8X@|@e[)S)*0m}97?OrBAeR');
define('AUTH_SALT',        'mI&8cmSom-%OqktXEW8Y870A(XS7Cq8k]q(4`nqhiBk?#R!!{kB}&A>F`eV~XHn,');
define('SECURE_AUTH_SALT', 'kR6QE`0}H_*40_40v##a!mFr9D0!Rolun*M[g3d~DX)|)8Bj(O^RmA^six#8U05g');
define('LOGGED_IN_SALT',   'M?I<>f6schb7 Rq0FlbW&Uc_,Y[_TgYz7heF~6LgHD L_~}@s&woNQ{r7?Ioy))1');
define('NONCE_SALT',       '+icazv`L4Cz%M(`Y=9w6CXU.W``y+u0+uk_U~P/y+yo1o%aCrZuVz;K; >O18g1]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
